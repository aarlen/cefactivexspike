﻿using System.Windows.Forms;
using System.Runtime.InteropServices;
using CefSharp;
using CefSharp.WinForms;
using System.Text;
using Microsoft.Win32;
using System.Reflection;

namespace CefActiveXControl
{
    [ProgId("CefActiveXSpike.CefUserControl1")]
    [Guid("F1ECBE9A-1C78-46BA-A9D8-38FB46BA9D19")]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public partial class CefUserControl : UserControl
    {
        public CefUserControl()
        {
            CefSettings settings = new CefSettings();
            settings.RemoteDebuggingPort = 9222;
            settings.CefCommandLineArgs.Add("--js-flags", "--expose-gc");
            Cef.Initialize(settings);


            InitializeComponent();

            var browser = new ChromiumWebBrowser("https://www.google.com");
            Controls.Add(browser);
            browser.Dock = DockStyle.Fill;
        }
    }
    /*static class Program
    {
        [ComRegisterFunction()]
        public static void RegisterClass(string key)
        {
            StringBuilder sb = new StringBuilder(key);
            sb.Replace(@"HKEY_CLASSES_ROOT\", "");

            // Open the CLSID\{guid} key for write access  

            RegistryKey k = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);

            RegistryKey ctrl = k.CreateSubKey("Control");
            ctrl.Close();

            // Next create the CodeBase entry - needed if not string named and GACced.  

            RegistryKey inprocServer32 = k.OpenSubKey("InprocServer32", true);
            inprocServer32.SetValue("CodeBase", Assembly.GetExecutingAssembly().CodeBase);
            inprocServer32.Close();

            k.Close();
        }

        [ComUnregisterFunction()]
        public static void UnregisterClass(string key)
        {
            StringBuilder sb = new StringBuilder(key);
            sb.Replace(@"HKEY_CLASSES_ROOT\", "");

            // Open HKCR\CLSID\{guid} for write access  

            RegistryKey k = Registry.ClassesRoot.OpenSubKey(sb.ToString(), true);

            // Delete the 'Control' key, but don't throw an exception if it does not exist  
            if (k == null)
            {
                return;
            }
            k.DeleteSubKey("Control", false);

            // Next open up InprocServer32  

            RegistryKey inprocServer32 = k.OpenSubKey("InprocServer32", true);

            // And delete the CodeBase key, again not throwing if missing   

            inprocServer32.DeleteSubKey("CodeBase", false);

            // Finally close the main key   

            inprocServer32.Close(); k.Close();
        }
    }*/
}
